console.log(`JSON`);


//json
/*console.log({
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
});*/

console.log(`This is a JavaScript Array:`);

let batchArr = [
	{
		name: `Angelito`,
	 	batch: `B163`,

	 },
	{
		name: `Ian`,
		batch: `B163`
	}
]

//to convert js objects & arrays to json, we will use stringify(arg)
console.log(batchArr);

batchArr.forEach(data=>console.log(data));

//convert javascript to JSON
const convertedToStr = JSON.stringify(batchArr);
console.log(convertedToStr);

//converted.forEach(data=>console.log(data));
	//not possible to use JS methods if the state is still in json/string

//convert JSON to javascript
//console.log(JSON.parse(coverted));
const convertedToJS = JSON.parse(convertedToStr);
convertedToJS.forEach(data=>console.log(data));